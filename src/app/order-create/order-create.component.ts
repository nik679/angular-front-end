import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-order-create',
  templateUrl: './order-create.component.html',
  styleUrls: ['./order-create.component.css'],
})
export class OrderCreateComponent implements OnInit {
  @Input() orderDetails = {
    id: 0,
    stockTicker: '',
    price: 0,
    volume: 0,
    buyOrSell: '',
    statusCode: 0,
    orderTime: new Date(),
  };

  constructor(public restApi: RestApiService, public router: Router) {}

  ngOnInit() {}

  addOrder() {
    this.restApi.createOrder(this.orderDetails).subscribe((data: {}) => {
      this.router.navigate(['/order-list']);
    });
  }
}
