import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderCreateComponent } from './order-create/order-create.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderEditComponent } from './order-edit/order-edit.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'order-list' },
  { path: 'create-order', component: OrderCreateComponent },
  { path: 'order-list', component: OrderListComponent },
  { path: 'order-edit/:id', component: OrderEditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
